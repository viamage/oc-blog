<?php namespace Viamage\Blog\Updates;

use October\Rain\Database\Updates\Migration;
use DbDongle;

class UpdateTimestampsNullable extends Migration
{
    public function up()
    {
        DbDongle::disableStrictMode();

        DbDongle::convertTimestamps('viamage_blog_posts');
        DbDongle::convertTimestamps('viamage_blog_categories');
    }

    public function down()
    {
        // ...
    }
}
