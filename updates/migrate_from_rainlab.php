<?php namespace Viamage\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Viamage\Blog\Models\Category;
use Viamage\Blog\Models\Post;

class MigrateFromRainlab extends Migration
{
    public function up()
    {
        if (Schema::hasTable('rainlab_blog_posts')) {
            \DB::table('viamage_blog_posts')->truncate();
            $posts = \DB::table('rainlab_blog_posts')->get();
            foreach ($posts as $post) {
                $newPost = new Post();
                $newPost->id = $post->id;
                $newPost->user_id = $post->user_id;
                $newPost->title = $post->title;
                $newPost->slug = $post->slug;
                $newPost->excerpt = $post->excerpt;
                $newPost->content = $post->content;
                $newPost->content_html = $post->content_html;
                $newPost->published_at = $post->published_at;
                $newPost->published = $post->published;
                $newPost->created_at = $post->created_at;
                $newPost->updated_at = $post->updated_at;
                $newPost->save();
            }
            \DB::table('viamage_blog_categories')->truncate();
            $categories = \DB::table('rainlab_blog_categories')->get();
            foreach ($categories as $category) {
                $newCategory = new Category();
                $newCategory->id = $category->id;
                $newCategory->name = $category->name;
                $newCategory->slug = $category->slug;
                $newCategory->code = $category->code;
                $newCategory->description = $category->description;
                $newCategory->parent_id = $category->parent_id;
                $newCategory->nest_left = $category->nest_left;
                $newCategory->nest_right = $category->nest_right;
                $newCategory->nest_depth = $category->nest_depth;
                $newCategory->created_at = $category->created_at;
                $newCategory->updated_at = $category->updated_at;
                $newCategory->color = $category->color;
                $newCategory->icon = $category->icon;
                $newCategory->save();
            }
            \DB::table('viamage_blog_posts_categories')->truncate();
            $postCats = \DB::table('rainlab_blog_posts_categories')->get();
            foreach ($postCats as $postCat) {
                \DB::table('viamage_blog_posts_categories')->insert(
                    [
                        'post_id'     => $postCat->post_id,
                        'category_id' => $postCat->category_id,
                    ]
                );
            }

            \DB::table('system_files')->where('attachment_type', 'RainLab\Blog\Models\Post')->update([
                'attachment_type' => 'Viamage\Blog\Models\Post'
            ]);
        }
    }

    public function down()
    {

    }
}
