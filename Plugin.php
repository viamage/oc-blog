<?php namespace Viamage\Blog;

use Backend;
use Controller;
use Viamage\Blog\Controllers\Posts;
use Viamage\Blog\Models\Post;
use System\Classes\PluginBase;
use Viamage\Blog\Classes\TagProcessor;
use Viamage\Blog\Models\Category;
use Event;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'viamage.blog::lang.plugin.name',
            'description' => 'viamage.blog::lang.plugin.description',
            'author'      => 'Alexey Bobkov, Samuel Georges',
            'icon'        => 'icon-pencil',
            'homepage'    => 'https://github.com/viamage/blog-plugin'
        ];
    }

    public function registerComponents()
    {
        return [
            \Viamage\Blog\Components\Post::class  => 'blogPost',
            \Viamage\Blog\Components\Posts::class => 'blogPosts',
            Components\Categories::class          => 'blogCategories',
            Components\RssFeed::class             => 'blogRssFeed',
            Components\BlogTags::class            => 'blogTags',
            Components\BlogTagSearch::class       => 'blogTagSearch',
            Components\BlogRelated::class         => 'blogRelated'
        ];
    }

    public function registerPermissions()
    {
        return [
            'viamage.blog.access_posts' => [
                'tab'   => 'viamage.blog::lang.blog.tab',
                'label' => 'viamage.blog::lang.blog.access_posts'
            ],
            'viamage.blog.access_categories' => [
                'tab'   => 'viamage.blog::lang.blog.tab',
                'label' => 'viamage.blog::lang.blog.access_categories'
            ],
            'viamage.blog.access_other_posts' => [
                'tab'   => 'viamage.blog::lang.blog.tab',
                'label' => 'viamage.blog::lang.blog.access_other_posts'
            ],
            'viamage.blog.access_import_export' => [
                'tab'   => 'viamage.blog::lang.blog.tab',
                'label' => 'viamage.blog::lang.blog.access_import_export'
            ],
            'viamage.blog.access_publish' => [
                'tab'   => 'viamage.blog::lang.blog.tab',
                'label' => 'viamage.blog::lang.blog.access_publish'
            ]
        ];
    }

    public function registerNavigation()
    {
        return [
            'blog' => [
                'label'       => 'viamage.blog::lang.blog.menu_label',
                'url'         => Backend::url('viamage/blog/posts'),
                'icon'        => 'icon-pencil',
                'iconSvg'     => 'plugins/viamage/blog/assets/images/blog-icon.svg',
                'permissions' => ['viamage.blog.*'],
                'order'       => 300,

                'sideMenu' => [
                    'new_post' => [
                        'label'       => 'viamage.blog::lang.posts.new_post',
                        'icon'        => 'icon-plus',
                        'url'         => Backend::url('viamage/blog/posts/create'),
                        'permissions' => ['viamage.blog.access_posts']
                    ],
                    'posts' => [
                        'label'       => 'viamage.blog::lang.blog.posts',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('viamage/blog/posts'),
                        'permissions' => ['viamage.blog.access_posts']
                    ],
                    'categories' => [
                        'label'       => 'viamage.blog::lang.blog.categories',
                        'icon'        => 'icon-list-ul',
                        'url'         => Backend::url('viamage/blog/categories'),
                        'permissions' => ['viamage.blog.access_categories']
                    ]
                ]
            ]
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'Viamage\Blog\FormWidgets\Preview' => [
                'label' => 'Preview',
                'code'  => 'preview'
            ]
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register()
    {
        /*
         * Register the image tag processing callback
         */
        TagProcessor::instance()->registerCallback(function($input, $preview) {
            if (!$preview) {
                return $input;
            }

            return preg_replace('|\<img src="image" alt="([0-9]+)"([^>]*)\/>|m',
                '<span class="image-placeholder" data-index="$1">
                    <span class="upload-dropzone">
                        <span class="label">Click or drop an image...</span>
                        <span class="indicator"></span>
                    </span>
                </span>',
            $input);
        });
    }

    public function boot()
    {
        /*
         * Register menu items for the Viamage.Pages plugin
         */
        Event::listen('pages.menuitem.listTypes', function() {
            return [
                'blog-category'       => 'viamage.blog::lang.menuitem.blog_category',
                'all-blog-categories' => 'viamage.blog::lang.menuitem.all_blog_categories',
                'blog-post'           => 'viamage.blog::lang.menuitem.blog_post',
                'all-blog-posts'      => 'viamage.blog::lang.menuitem.all_blog_posts',
                'category-blog-posts' => 'viamage.blog::lang.menuitem.category_blog_posts',
            ];
        });

        Event::listen('pages.menuitem.getTypeInfo', function($type) {
            if ($type === 'blog-category' || $type === 'all-blog-categories') {
                return Category::getMenuTypeInfo($type);
            }

            if ($type === 'blog-post' || $type === 'all-blog-posts' || $type === 'category-blog-posts') {
                return Post::getMenuTypeInfo($type);
            }
        });

        Event::listen('pages.menuitem.resolveItem', function($type, $item, $url, $theme) {
            if ($type === 'blog-category' || $type === 'all-blog-categories') {
                return Category::resolveMenuItem($item, $url, $theme);
            }

            if ($type === 'blog-post' || $type === 'all-blog-posts' || $type === 'category-blog-posts') {
                return Post::resolveMenuItem($item, $url, $theme);
            }
        });

        // extend the blog navigation
        Event::listen('backend.menu.extendItems', function($manager) {
            $manager->addSideMenuItems('Viamage.Blog', 'blog', [
                'tags' => [
                    'label' => 'viamage.blog::lang.navigation.tags',
                    'icon'  => 'icon-tags',
                    'code'  => 'tags',
                    'owner' => 'Viamage.Blog',
                    'url'   => Backend::url('viamage/blog/tags')
                ]
            ]);
        });

        // extend the post model
        Post::extend(function($model) {
            $model->belongsToMany['tags'] = [
                'Viamage\Blog\Models\Tag',
                'table' => 'viamage_blog_post_tag',
                'order' => 'name'
            ];
        });

        // extend the post form
        Posts::extendFormFields(function($form, $model, $context) {
            if (!$model instanceof Post) {
                return;
            }

            $form->addSecondaryTabFields([
                'tags' => [
                    'label' => 'bedard.blogtags::lang.form.label',
                    'mode'  => 'relation',
                    'tab'   => 'rainlab.blog::lang.post.tab_categories',
                    'type'  => 'taglist'
                ]
            ]);
        });
    }
}
